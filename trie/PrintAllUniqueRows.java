package trie;

import java.util.ArrayList;
import java.util.Arrays;

public class PrintAllUniqueRows {

    public static void printAllUniqueRowsOfBooleanMatrix(int[][] matrix){
        TrieNode root = new TrieNode(2);
        for (int[] row : matrix) {
            if(insert(row, root)){
                printRow(row);
            }
        }
    }

    private static void printRow(int[] row){
        for (int j : row) {
            System.out.print(j);
            System.out.print(",");
        }
        System.out.println();
    }

    private static boolean insert(int[] row, TrieNode root){
        boolean newNodeCreated = false;
        TrieNode currentNode = root;
        for (int j : row) {
            if (currentNode.children[j] == null) {
                newNodeCreated = true;
                currentNode.children[j] = new TrieNode(2);
            }
            currentNode = currentNode.children[j];
        }
        currentNode.isWordEndHere = true;
        return newNodeCreated;
    }
}
