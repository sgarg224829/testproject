package trie;

public class TrieNode {

    TrieNode[] children;
    boolean isWordEndHere;

    public TrieNode(int n){
        children = new TrieNode[n];
        isWordEndHere = false;
    }
}
