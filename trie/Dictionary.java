package trie;

import java.util.*;
import java.util.stream.Collectors;

public class Dictionary {

    private final TrieNode root;

    public Dictionary(){
        this.root = new TrieNode(26);
    }

    public void addWord(String word){
        TrieNode currentNode = root;
        char[] charArr = word.toCharArray();
        for (char ch : charArr) {
            if (currentNode.children[ch - 'a'] == null) {
                currentNode.children[ch - 'a'] = new TrieNode(26);
            }
            currentNode = currentNode.children[ch - 'a'];
        }
        currentNode.isWordEndHere = true;
    }

    public boolean deleteWord(String word){
        TrieNode currentNode = root;
        TrieNode lastNode = null;
        char lastChar = 'a';
        for(char ch : word.toCharArray()){
            int index = ch - 'a';
            if(currentNode.children[index] == null){
                return false;
            }else{
                if(!allOtherChildrenEmpty(currentNode.children, index)){
                    lastNode = currentNode;
                    lastChar = ch;
                }
                currentNode = currentNode.children[index];
            }
        }

        if(currentNode.children[0] == null && allOtherChildrenEmpty(currentNode.children, 0)){
            currentNode.isWordEndHere = false;
            return true;
        }

        if(lastNode != null){
            lastNode.children[lastChar - 'a'] = null;
            return true;
        }else{
            root.children[word.toCharArray()[0] - 'a'] = null;
            return true;
        }
    }

    private boolean allOtherChildrenEmpty(TrieNode[] arr, int index){
        boolean allEmpty = true;
        for(int i = 0; i < arr.length ; i++ ){
            if(i != index){
                if(arr[i] != null){
                    allEmpty = false;
                    return allEmpty;
                }
            }
        }
        return allEmpty;
    }

    public boolean searchWord(String word){
        TrieNode currentNode = root;
        for(char ch : word.toCharArray()){
            int index = ch - 'a';
            if(currentNode.children[index] == null){
                return false;
            }
            currentNode = currentNode.children[index];
        }
        return currentNode.isWordEndHere;
    }

    public boolean searchWordWithPrefix(String prefix){
        TrieNode currentNode = root;
        for(char ch : prefix.toCharArray()){
            int index = ch - 'a';
            if(currentNode.children[index] == null){
                return false;
            }
            currentNode = currentNode.children[index];
        }
        return true;
    }

    public void printAllWords(){
        getAllWords(root).forEach(System.out::println);
    }

    public void printAllWordsWithPrefix(String prefix){
        getAllWordsWithPrefix(root, prefix).forEach(System.out::println);
    }

    public List<String> getAllWordsWithPrefix(TrieNode root, String prefix){
        List<String> result = new ArrayList<>();
        TrieNode currentNode = root;
        for(char c : prefix.toCharArray()){
            int index = c - 'a';
            if(currentNode.children[index] == null){
                return result;
            }
            currentNode = currentNode.children[index];
        }
        getAllWords(currentNode).forEach(word -> result.add(prefix.concat(word)));
        return result;
    }

    private List<String> getAllWords(TrieNode root){
        List<String> result = new ArrayList<>();
        for(int i = 0 ; i< root.children.length ; i++){
            char c = (char) ('a' + (char)i);
            if(root.children[i] != null){
                if (root.children[i].isWordEndHere) {
                    result.add(String.valueOf(c));
                }
                List<String> childrenWords = getAllWords(root.children[i]);
                for(String childrenWord : childrenWords){
                    String concatWord = String.valueOf(c).concat(childrenWord);
                    result.add(concatWord);
                }
            }
        }
        return result;
    }

    public boolean searchCombination(String s){
        boolean ans = false;
        TrieNode currNode = root;
        for(int i = 0; i < s.length() ; i++){
            int index = s.charAt(i) - 'a';
            if(currNode.children[index] != null){
                if(currNode.children[index].isWordEndHere){
                    currNode = root;
                    ans = true;
                }else{
                    currNode = currNode.children[index];
                }
            }else {
                ans = false;
            }
        }
        return ans;
    }
}
