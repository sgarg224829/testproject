package trie;

import java.util.*;
import java.util.stream.Collectors;

public class GroupByAnagrams {

    public static List<ArrayList<String>> groupAnagramsTogether(ArrayList<String> strList) {
        HashMap<String, ArrayList<String>> map = new HashMap<>();
        for(String word : strList){
            char[] wordsArr = word.toCharArray();
            Arrays.sort(wordsArr);
            String sortedWord = String.valueOf(wordsArr);

            ArrayList<String> anagrams;
            if(!map.containsKey(sortedWord)){
                anagrams = new ArrayList<>();
                map.put(sortedWord, anagrams);
            }
            anagrams = map.get(sortedWord);
            anagrams.add(word);
        }

        map.values().forEach(Collections::sort);
        return map.values().stream().sorted(Comparator.comparing(o -> o.get(0))).collect(Collectors.toList());
    }
}
