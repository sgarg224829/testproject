package heap;

import java.util.ArrayList;
import java.util.List;

public class MaxHeap {

    private final List<Integer> list;
    private int currentIndex;

    public MaxHeap(){
        this.list = new ArrayList<>();
        this.currentIndex = 0;
    }

    public void insertKey(int data){
        list.add(data);
        currentIndex++;
        heapifyFromEnd(currentIndex - 1);
    }

    public void deleteMaxKey(){
        swap(0, currentIndex-1);
        currentIndex--;
        heapifyFromStart(0);
    }

    public void decreaseKey(int index, int newValue){
        list.set(index, newValue);
        heapifyFromStart(index);
    }

    public void increaseKey(int index, int newValue){
        list.set(index, newValue);
        heapifyFromEnd(index);
    }

    public void modifyKey(int index, int newValue){
        int data = list.get(index);
        if(data < newValue){
            increaseKey(data, newValue);
        }else if(data > newValue){
            decreaseKey(data, newValue);
        }
    }

    public void deleteKey(int index){
        increaseKey(index, Integer.MAX_VALUE);
        deleteMaxKey();
    }

    public int getMax(){
        if(currentIndex != 0){
            return list.get(0);
        }
        return -1;
    }

    public void heapifyArray(){
        boolean isHeap = false;
        while (!isHeap){
            int i = 0;
            boolean anySwaps = false;
            while (i <= (currentIndex-1)/2){

                int leftIndex = 2 * i + 1;
                int rightIndex = 2 * i + 2;

                int left = -1, right = -1;
                if(leftIndex < currentIndex){
                    left = list.get(leftIndex);
                }
                if(rightIndex < currentIndex){
                    right = list.get(rightIndex);
                }

                int max = Math.max(left, right);
                if(max > list.get(i)){
                    anySwaps = true;
                    if(max == left){
                        swap(i, leftIndex);
                    }else{
                        swap(i, rightIndex);
                    }
                }

                i++;
            }
            isHeap = !anySwaps;
        }
    }

    public void heapifyFromEnd(int index){
        while (index > 0){
            int parentIndex = (index-1)/2;
            if(list.get(parentIndex) < list.get(index)){
                swap(parentIndex, index);
                index = parentIndex;
            }else{
                break;
            }
        }
    }

    public void heapifyFromStart(int index){
        while (index < currentIndex) {
            int leftIndex = 2 * index + 1;
            int rightIndex = 2 * index + 1;

            int left = -1, right = -1;
            if(leftIndex < currentIndex){
                left = list.get(leftIndex);
            }
            if(rightIndex < currentIndex){
                right = list.get(rightIndex);
            }

            int max = Math.max(left, right);
            if(list.get(index) < max){
                if(max == left){
                    swap(index, leftIndex);
                    index = leftIndex;
                }else{
                    swap(index, rightIndex);
                    index = rightIndex;
                }
            }else {
                break;
            }
        }
    }

    public void swap(int i , int j){
        int temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }
}
