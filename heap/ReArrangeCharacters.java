package heap;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class ReArrangeCharacters {

    static class HeapNode implements Comparable<HeapNode>{
        char c;
        int freq;

        public HeapNode(char c, int freq){
            this.c = c;
            this.freq = freq;
        }

        @Override
        public int compareTo(HeapNode o) {
            return o.freq - this.freq;
        }
    }

    public static String reArrangeCharacters(String s){
        Map<Character, Integer> map = new HashMap<>();
        for(char ch: s.toCharArray()){
            if(map.containsKey(ch)){
                int freq = map.get(ch);
                map.put(ch, freq + 1);
            }else{
                map.put(ch, 1);
            }
        }

        PriorityQueue<HeapNode> maxHeap = new PriorityQueue<>(map.keySet().size());
        for (Map.Entry<Character, Integer> entry : map.entrySet()){
            HeapNode node = new HeapNode(entry.getKey(), entry.getValue());
            maxHeap.add(node);
        }

        StringBuilder ans = new StringBuilder();
        HeapNode prevNode = null;
        while (!maxHeap.isEmpty()){
            HeapNode node = maxHeap.poll();
            if(prevNode != null){
                maxHeap.add(prevNode);
            }
            if(node.freq > 0){
                ans.append(node.c);
                node.freq = node.freq - 1;
                if(node.freq > 0){
                    prevNode = node;
                }else{
                    prevNode = null;
                }
            }
        }
        return ans.length() == s.length() ? ans.toString() : "Not Possible";
    }
}
