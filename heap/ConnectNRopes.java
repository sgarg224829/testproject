package heap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class ConnectNRopes {

    public static int minimumCost(int[] ropes, int n){
        int cost = 0;
        PriorityQueue<Integer> minHeap = new PriorityQueue<>(n);
        for(int rope : ropes){
            minHeap.add(rope);
        }

        while (!minHeap.isEmpty()){
            int first = minHeap.poll();
            if(!minHeap.isEmpty()){
                int second = minHeap.poll();
                cost = cost + first + second;
                minHeap.add(first+second);
            }
        }
        return cost;
    }

    public static long minCost(long[] arr, int n){
        List<Long> list = new ArrayList<>();
        for(long rope : arr){
            list.add(rope);
        }
        return minCost(list, n);
    }

    public static long minCost(List<Long> arr, int n)
    {
        long cost = 0;
        PriorityQueue<Long> maxHeap = new PriorityQueue<>(3, Comparator.reverseOrder());
        for(int j = 0 ; j < n ; j++){
            maxHeap.add(arr.get(j));
            if(maxHeap.size() > 2){
                maxHeap.poll();
            }
        }

        long first = maxHeap.poll();
        if(!maxHeap.isEmpty()){
            long second = maxHeap.poll();
            cost = cost + first + second;
            arr.remove(first);
            arr.remove(second);
            arr.add(first + second);
        }

        return cost + (((n-1) > 1) ? minCost(arr, n-1) : 0);
    }
}
