package heap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Set;

public class KMaxSumCombinations {

    static class PairSum implements  Comparable<PairSum>{
        int sum;
        int i;
        int j;

        public PairSum(int sum, int i , int j){
            this.sum = sum;
            this.i = i;
            this.j = j;
        }

        @Override
        public int compareTo(PairSum o) {
            return o.sum - this.sum;
        }
    }

    static class Pair{
        int i;
        int j;

        public Pair(int i , int j){
            this.i = i;
            this.j = j;
        }

        @Override
        public boolean equals(Object obj) {
            if(obj == null){
                return false;
            }else if(obj instanceof Pair pair){
                return pair.i == this.i && pair.j == this.j;
            }else {
                return false;
            }
        }

        @Override
        public int hashCode() {
            return Objects.hash(i, j);
        }
    }

    public static List<Integer> maxCombinations(int N, int K, int A[], int B[]) {
        List<Integer> ans = new ArrayList<>();
        Arrays.sort(A);
        Arrays.sort(B);
        Set<Pair> pairs = new HashSet<>();
        PriorityQueue<PairSum> maxHeap = new PriorityQueue<>(N);

        maxHeap.add(new PairSum(A[N-1] + B[N-1], N-1, N-1));
        pairs.add(new Pair(N-1, N-1));

        for(int p = 0 ; p< K ; p++){
            PairSum pairSum = maxHeap.poll();
            ans.add(pairSum.sum);

            int i = pairSum.i;
            int j = pairSum.j-1;
            Pair pair = new Pair(i, j);

            if(i >=0 && j>= 0 & !pairs.contains(pair)){
                maxHeap.add(new PairSum(A[i] + B[j], i, j));
                pairs.add(pair);
            }

            i = pairSum.i - 1;
            j = pairSum.j;
            pair = new Pair(i, j);

            if(i >=0 && j>= 0 & !pairs.contains(pair)){
                maxHeap.add(new PairSum(A[i] + B[j], i, j));
                pairs.add(pair);
            }
        }

        return ans;
    }
}
