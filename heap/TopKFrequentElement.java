package heap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Stack;

public class TopKFrequentElement {

    static class HeapNode implements Comparable<HeapNode>{
        int data;
        int freq;
        public HeapNode(int data, int freq){
            this.data = data;
            this.freq = freq;
        }

        @Override
        public int compareTo(HeapNode o) {
            if(this.freq != o.freq){
                return this.freq - o.freq;
            }else{
                return this.data - o.data;
            }
        }
    }
    public static int[] topKFrequentElement(int[] arr, int k){
        Map<Integer, Integer> freqMap = new HashMap<>();
        for (int elem : arr){
            if(freqMap.containsKey(elem)){
                int freq = freqMap.get(elem);
                freqMap.put(elem, ++freq);
            }else{
                freqMap.put(elem, 1);
            }
        }

        PriorityQueue<HeapNode> minHeap = new PriorityQueue<>(k+1);
        for(Map.Entry<Integer, Integer> entry : freqMap.entrySet()){
            HeapNode node = new HeapNode(entry.getKey(), entry.getValue());
            minHeap.add(node);
            if(minHeap.size() > k){
                minHeap.poll();
            }
        }

        Stack<Integer> stack = new Stack<>();
        while (!minHeap.isEmpty()){
            stack.push(minHeap.poll().data);
        }

        int[] ans = new int[k];
        int i = 0;
        while (!stack.isEmpty()){
            ans[i++] = stack.pop();
        }

        return ans;
    }
}
