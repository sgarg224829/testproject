package heap;

import java.util.LinkedList;

public class SlidingWindowMaximum {

    public static int[] slidingWindowMaximum(int[] arr, int k){

        //[5, 8 ,7, 3, 4, 6, 10, 1]

        int[] ans = new int[arr.length - 2];
        LinkedList<Integer> queue = new LinkedList<>();
        int i, j = 0;

        for(i = 0; i< k ; i++){

            while (!queue.isEmpty() && arr[queue.peekLast()] <= arr[i]){
                queue.removeLast();
            }

            queue.add(i);
        }

        for ( ; i< arr.length ; i++){
            ans[j++] = arr[queue.peek()];

            while (!queue.isEmpty() && queue.peek() <= i-k){
                queue.remove();
            }

            while (!queue.isEmpty() && arr[queue.peekLast()] <= arr[i]){
                queue.removeLast();
            }

            queue.add(i);
        }

        ans[j++] = arr[queue.peek()];
        return ans;
    }
}
