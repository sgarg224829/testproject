package heap;

import java.util.Arrays;

public class MinimumSum {

    public static long minSumOfTwoNumbers(int[] digits){
        Arrays.sort(digits);
        long num1 = 0;
        long num2 = 0;
        for(int i=0; i< digits.length ;){
            num1 = num1 * 10 + digits[i];
            if(i + 1 < digits.length){
                num2 = num2 * 10 + digits[i+1];
            }
            i = i+2;
        }
        return num1 + num2;
    }
}
