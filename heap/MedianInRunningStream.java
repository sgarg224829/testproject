package heap;

import java.util.PriorityQueue;

public class MedianInRunningStream {

    public static int[] findMedianInRunningStream(int[] arr, int n){
        int[] ans = new int[n];
        int j = 1;

        int root = arr[0];
        ans[0] = root;
        PriorityQueue<Integer> left = new PriorityQueue<>((n/2) + 1, (i1, i2) -> i2-i1);
        PriorityQueue<Integer> right = new PriorityQueue<>((n/2) + 1);
        for(int i =1 ; i< n ; i++){
            if(arr[i] < root){
                left.add(arr[i]);
                if(left.size() - right.size() == 2){
                    int leftMax = left.poll();
                    right.add(root);
                    root = leftMax;
                }
            }else{
                right.add(arr[i]);
                if(right.size() - left.size() == 2){
                    int rightMin = right.poll();
                    left.add(root);
                    root = rightMin;
                }
            }

            if(i%2 == 0){
                ans[j++] = root;
            }else{
                ans[j++] = left.size() > right.size() ? (root + left.peek())/2 : (root + right.peek())/2;
            }
        }
        return ans;
    }
}
