package heap;

import java.util.TreeMap;

public class StraightHands {

    public static boolean isStraightHand(int N, int groupSize, int hand[]) {

        if(N % groupSize != 0){
            return false;
        }

        TreeMap<Integer, Integer> map = new TreeMap<>();
        for(int elem : hand){
            map.put(elem, map.getOrDefault(elem, 0) +1);
        }

        while (!map.isEmpty()){
            int first = map.firstKey();
            for (int j = 1; j < groupSize; j++){
                if(map.containsKey(first + j)){
                    map.put(first + j, map.get(first+j) -1);
                    if(map.get(first + j) == 0){
                        map.remove(first + j);
                    }
                }else {
                    return false;
                }
            }

            map.put(first , map.get(first) -1);
            if(map.get(first) == 0){
                map.remove(first);
            }
        }
        return true;
    }
}
