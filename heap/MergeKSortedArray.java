package heap;

import LinkedList.LLNode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

public class MergeKSortedArray {

    static class Structure implements Comparable<Structure>{
        int value;
        int row;
        int column;

        public Structure(int value, int row, int column){
            this.value = value;
            this.row = row;
            this.column = column;
        }

        @Override
        public int compareTo(Structure o) {
            return this.value - o.value;
        }
    }
    public static int[] mergeKSortedArray(int[][] kArrays, int k , int n){
        int[] ans = new int[k*n];
        PriorityQueue<Structure> minHeap = new PriorityQueue<>(k);
        Map<Integer, Integer> keyIndex = new HashMap<>();
        for(int i=0 ; i<k ; i++){
            minHeap.add(new Structure(kArrays[i][0], i, 0));
            keyIndex.put(i, 0);
        }

        int j = 0;
        while (!minHeap.isEmpty()){
            Structure min = minHeap.poll();
            ans[j++] = min.value;

            if(keyIndex.get(min.row) + 1 > n){
                min = minHeap.peek();
            }

            int row = min.row;
            int column = keyIndex.get(row) + 1;
            if(row < k && column < n){
                minHeap.add(new Structure(kArrays[row][column], row, column));
                keyIndex.put(min.row, column);
            }

        }

        return ans;
    }

    public static LLNode mergeKSortedLinkedLists(List<LLNode> linkedLists) {
        int k = linkedLists.size();
        if(k == 0){
            return null;
        }else if(k == 1){
            return linkedLists.get(0);
        }else if(k == 2){
            return mergeTwoSortedLLinkedLists(linkedLists.get(0), linkedLists.get(1));
        }else{
            LLNode head1 = mergeKSortedLinkedLists(linkedLists.subList(0, k/2));
            LLNode head2 = mergeKSortedLinkedLists(linkedLists.subList(k/2, k));
            return mergeTwoSortedLLinkedLists(head1, head2);
        }
    }

    public static LLNode mergeTwoSortedLLinkedLists(LLNode head1, LLNode head2){
        if(head1 != null && head2 != null){
            LLNode ans;
            if(head1.data < head2.data){
                ans = head1;
                ans.next = mergeTwoSortedLLinkedLists(head1.next, head2);
            }else{
                ans = head2;
                ans.next = mergeTwoSortedLLinkedLists(head1, head2.next);
            }
            return ans;
        }else if(head1 == null && head2 == null){
            return null;
        }else {
            return Objects.requireNonNullElse(head1, head2);
        }
    }
}
