package heap;

public class ConvertMinHeapToMax {

    public static void convertMinToMaxHeap(int N, int[] arr) {
        boolean isHeap = false;
        int currentIndex = N;
        while (!isHeap){
            int i = 0;
            boolean anySwaps = false;
            while (i <= (currentIndex-1)/2){

                int leftIndex = 2 * i + 1;
                int rightIndex = 2 * i + 2;

                int left = -1, right = -1;
                if(leftIndex < currentIndex){
                    left = arr[leftIndex];
                }
                if(rightIndex < currentIndex){
                    right = arr[rightIndex];
                }

                int max = Math.max(left, right);
                if(max > arr[i]){
                    anySwaps = true;
                    if(max == left){
                        swap(arr, i, leftIndex);
                    }else{
                        swap(arr, i, rightIndex);
                    }
                }

                i++;
            }
            isHeap = !anySwaps;
        }
    }

    public static void swap(int[] arr, int i , int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
