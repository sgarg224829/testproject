package heap;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class TaskScheduler {

    static class HeapNode implements Comparable<HeapNode>{
        char c;
        int freq;

        public HeapNode(char c, int freq){
            this.c = c;
            this.freq = freq;
        }

        @Override
        public int compareTo(HeapNode o) {
            if(o.freq == this.freq){
                return this.c - o.c;
            }else {
                return o.freq - this.freq;
            }
        }
    }

    public static int leastInterval(char[] tasks, int n) {
        if(n == 0){
            return tasks.length;
        }

        Map<Character, Integer> map = new HashMap<>();
        for(char c : tasks){
            map.put(c, map.getOrDefault(c, 0) + 1);
        }

        PriorityQueue<HeapNode> maxHeap = new PriorityQueue<>(map.keySet().size());
        for(Map.Entry<Character, Integer> entry : map.entrySet()){
            HeapNode node = new HeapNode(entry.getKey(), entry.getValue());
            maxHeap.add(node);
        }

        StringBuilder s = new StringBuilder();
        while (!maxHeap.isEmpty()){
            List<HeapNode> used = new ArrayList<>();
            HeapNode node = maxHeap.poll();
            s.append(node.c);
            used.add(node);
            for(int i = 1; i <= n ; i++){
                if(!maxHeap.isEmpty()){
                    node = maxHeap.poll();
                    s.append(node.c);
                    used.add(node);
                }else{
                    s.append('_');
                }
            }
            for (HeapNode heapNode : used){
                heapNode.freq = heapNode.freq -1;
                if(heapNode.freq > 0){
                    maxHeap.add(heapNode);
                }
            }
        }

        for(int j = s.length()-1 ; j > 0; j--){
            if(s.charAt(j) == '_'){
                s.deleteCharAt(j);
            }else{
                break;
            }
        }

        return s.length();
    }

    public static int leastInterval(int n, int K, char tasks[]) {
        // creating an array to store the frequencies of tasks
        int[] counter = new int[26];
        // variable to store the maximum frequency
        int max = 0;
        // variable to store the number of tasks with maximum frequency
        int maxCount = 0;

        // iterating over the tasks array
        for (char task : tasks) {
            // incrementing the frequency of the current task
            counter[task - 'A']++;
            // checking if the current task has the maximum frequency so far
            // and updating max and maxCount accordingly
            if (max == counter[task - 'A']) {
                maxCount++;
            } else if (max < counter[task - 'A']) {
                max = counter[task - 'A'];
                maxCount = 1;
            }
        }

        // calculating the number of parts and their lengths
        int partCount = max - 1;
        int partLength = K - (maxCount - 1);

        // calculating the number of empty slots, available tasks, and idles
        int emptySlots = partCount * partLength;
        int availableTasks = tasks.length - max * maxCount;
        int idles = Math.max(0, emptySlots - availableTasks);

        // returning the total number of tasks and idles
        return tasks.length + idles;
    }
}
