package heap;

import java.util.PriorityQueue;

public class KthLargestSumForSubArrays {

    public static int kthLargestSumForSubArrays(int[] arr, int k){
        int i, j;
        for (i = 1; i < arr.length ; i++) {
            arr[i] = arr[i-1] + arr[i];
        }

        PriorityQueue<Integer> minHeap = new PriorityQueue<>(k+1);
        for(i = 0; i< arr.length ; i++){
            for(j = i; j < arr.length ; j++){
                if(i == 0){
                    minHeap.add(arr[j]);
                }else{
                    minHeap.add(arr[j] - arr[i-1]);
                }
                if(minHeap.size() > k){
                    minHeap.poll();
                }
            }
        }


        return minHeap.size() > 0 ? minHeap.peek() : -1;
    }
}
