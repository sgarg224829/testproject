package heap;

import java.lang.reflect.Array;

public class MinHeap<T extends Comparable<? super T>> {

    T[] arr;
    int currentIndex;

    int maxCapacity;

    public MinHeap(Class<T> clazz, int maxCapacity){
        this.arr = (T[]) Array.newInstance(clazz, maxCapacity);
        this.currentIndex = 0;
        this.maxCapacity = maxCapacity;
    }

    public void insertKey(T key){
        if(currentIndex == maxCapacity){
            throw new RuntimeException("Heap is full");
        }

        arr[currentIndex] = key;
        heapifyFromEnd(currentIndex);
        currentIndex++;
    }

    public T deleteMinKey(){
        if(currentIndex == 0){
            throw  new RuntimeException("Heap is Empty");
        }

        T firstElement = arr[0];
        swap(0, currentIndex-1);
        currentIndex--;
        heapifyFromStart(0);
        return firstElement;
    }

    public T getMinKey(){
        if(currentIndex == 0){
            throw  new RuntimeException("Heap is Empty");
        }

        return arr[0];
    }

    private void heapifyFromEnd(int index){
        while(index  > 0){
            int parentIndex = (index-1)/2;
            if(arr[index].compareTo(arr[parentIndex]) < 0){
                swap(index, parentIndex);
                index = parentIndex;
            }else {
                break;
            }
        }
    }

    private void heapifyFromStart(int index){
        while(index < currentIndex){
            int leftIndex = 2*index +1;
            int rightIndex = 2*index +2;

            T left = null, right = null;
            if(leftIndex < currentIndex){
                left = arr[leftIndex];
            }
            if(rightIndex < currentIndex){
                right = arr[rightIndex];
            }

            T max;
            if(left == null && right == null){
                break;
            }else if (left == null){
                max = right;
            } else if (right == null) {
                max = left;
            }else {
                max = left.compareTo(right) <=0 ? left : right;
            }
            if(arr[index].compareTo(max) > 0){
                if(max == left){
                    swap(index, leftIndex);
                    index = leftIndex;
                }else {
                    swap(index, rightIndex);
                    index = rightIndex;
                }
            }else {
                break;
            }
        }
    }

    public void swap(int i , int j){
        T temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
