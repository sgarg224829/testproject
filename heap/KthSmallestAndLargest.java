package heap;

import java.util.PriorityQueue;

public class KthSmallestAndLargest {

    public static Integer kthSmallest(int[] arr, int k){
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(k, (o1, o2) -> o2-o1);
        for (int elem : arr) {
            maxHeap.add(elem);
            if (maxHeap.size() > k) {
                maxHeap.poll();
            }
        }
        return maxHeap.peek();
    }

    public static Integer kthLargest(int[] arr, int k){
        PriorityQueue<Integer> minHeap = new PriorityQueue<>(k);
        for (int elem : arr) {
            minHeap.add(elem);
            if (minHeap.size() > k) {
                minHeap.poll();
            }
        }
        return minHeap.peek();
    }
}
