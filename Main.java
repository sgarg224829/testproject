import heap.ConnectNRopes;
import heap.KMaxSumCombinations;
import heap.KthLargestSumForSubArrays;
import heap.KthSmallestAndLargest;
import heap.MaxHeap;
import heap.MinHeap;
import heap.MinimumSum;
import heap.ReArrangeCharacters;
import heap.SlidingWindowMaximum;
import heap.TopKFrequentElement;
import tree.TreeNode;
import tree.TreeOperations;
import tree.TreeTraversal;
import tree.binarySearchTree.*;
import tree.binaryTree.*;
import trie.Dictionary;
import trie.PrintAllUniqueRows;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        //graphMain();
        //treeMain();
        //trieMain();
        //bstMain();
        //heapMain();
        heapProblems();
    }

    public static void heapProblems(){
        int[] arr = new int[]{15,9,28,17,34,4,18,25};
        System.out.println(KthSmallestAndLargest.kthSmallest(arr, 3));
        System.out.println(KthSmallestAndLargest.kthLargest(arr, 3));
        System.out.println(ReArrangeCharacters.reArrangeCharacters("aa"));
        System.out.println(MinimumSum.minSumOfTwoNumbers(new int[]{4,2,1,8,3}));
        System.out.println(KthLargestSumForSubArrays.kthLargestSumForSubArrays(new int[]{10,-10,20,-40}, 6));

        int[] ans = TopKFrequentElement.topKFrequentElement(new int[]{5,9,7,5,9,10,5,4,7,9,12,10,3,7,5}, 4);
        for (int i = 0; i < ans.length; i++) {
            System.out.print(ans[i] + " ");
        }
        System.out.println();
        System.out.println(ConnectNRopes.minCost(new long[]{1, 2, 3}, 3));

        ans = SlidingWindowMaximum.slidingWindowMaximum(new int[]{5, 8 ,7, 3, 4, 6, 10, 1}, 3);
        for (int i = 0; i < ans.length; i++) {
            System.out.print(ans[i] + " ");
        }
        System.out.println();
        System.out.println(KMaxSumCombinations.maxCombinations(2, 2, new int[]{3,2}, new int[]{1,4}));
    }

    public static void heapMain(){
        MaxHeap maxHeap = new MaxHeap();
        maxHeap.insertKey(24);
        maxHeap.insertKey(17);
        maxHeap.insertKey(20);
        maxHeap.insertKey(14);
        maxHeap.insertKey(28);
        maxHeap.insertKey(13);
        maxHeap.insertKey(19);
        maxHeap.insertKey(32);
        System.out.println(maxHeap.getMax());
        maxHeap.deleteMaxKey();
        System.out.println(maxHeap.getMax());

        MinHeap<Integer> minHeap = new MinHeap<>(Integer.class, 100);
        minHeap.insertKey(24);
        minHeap.insertKey(17);
        minHeap.insertKey(20);
        minHeap.insertKey(14);
        minHeap.insertKey(28);
        minHeap.insertKey(13);
        minHeap.insertKey(19);
        minHeap.insertKey(32);
        System.out.println(minHeap.getMinKey());
        minHeap.deleteMinKey();
        System.out.println(minHeap.getMinKey());
    }

    public static void bstMain(){
        List<Integer> preOrder = Arrays.asList(50,30,40,70,60,80);
        List<Integer> inOrder = Arrays.asList(30,40,50,60,70,80);
        TreeNode constructedTreeRoot = ConstructTreeFromTraversal.constructTreeFromPreOrderAndInorderTraversals(preOrder, inOrder);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(constructedTreeRoot));
        System.out.println(TreeTraversal.inOrderTraversal(BSTOperations.deleteFromTree(constructedTreeRoot, 50)));

        List<Integer> preOrder1 = Arrays.asList(4851431, 2267178, 8685515);
        List<Integer> inOrder1 = Arrays.asList(2267178, 4851431, 8685515);
        TreeNode constructedTreeRoot1 = ConstructTreeFromTraversal.constructTreeFromPreOrderAndInorderTraversals(preOrder1, inOrder1);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(constructedTreeRoot1));
        System.out.println(InOrderPredecessorAndSuccessor.predecessorSuccessor(constructedTreeRoot1, 6001049));

        List<Integer> preOrder2 = Arrays.asList(8, 5, 1, 7, 10, 12, 11);
        TreeNode constructedTreeRoot2 = ConstructBSTFromPreOrder.constructBSTFromPreOrder(preOrder2);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(constructedTreeRoot2));
        System.out.println(PreOrderToPostOrderConversion.preToPostConversion(preOrder2));

        List<Integer> preOrder3 = Arrays.asList(11,7,29,38,50,42,46,52,51);
        TreeNode constructedTreeRoot3 = ConstructBSTFromPreOrder.constructBSTFromPreOrder(preOrder3);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(constructedTreeRoot3));
        System.out.println(KthLargestElement.kthLargest(constructedTreeRoot3, 5));

        List<Integer> preOrder4 = Arrays.asList(1,2,4,5,3);
        List<Integer> inOrder4 = Arrays.asList(4,2,5,1,3);
        TreeNode constructedTreeRoot4 = ConstructTreeFromTraversal.constructTreeFromPreOrderAndInorderTraversals(preOrder4, inOrder4);
        System.out.println(InOrderIterative.inOrderIterative(constructedTreeRoot4));

        List<Integer> preOrder5 = Arrays.asList(5,3,2,4,6);
        List<Integer> preOrder6 = Arrays.asList(2,1,3,7,6);
        TreeNode constructedTreeRoot5 = ConstructBSTFromPreOrder.constructBSTFromPreOrder(preOrder5);
        TreeNode constructedTreeRoot6 = ConstructBSTFromPreOrder.constructBSTFromPreOrder(preOrder6);
        System.out.println(MergeBST.mergeBSTUsingIterativeInOrder(constructedTreeRoot5, constructedTreeRoot6));

        List<Integer> preOrder7 = Arrays.asList(7,4,9,6);
        List<Integer> inOrder7 = Arrays.asList(4,9,7,6);
        TreeNode constructedTreeRoot7 = ConstructTreeFromTraversal.constructTreeFromPreOrderAndInorderTraversals(preOrder7, inOrder7);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(constructedTreeRoot7));
        System.out.println(LargestBST.largestBst(constructedTreeRoot7));
    }

    public static void trieMain(){
        Dictionary dictionary = new Dictionary();
        dictionary.addWord("acb");
        dictionary.addWord("ac");
        dictionary.addWord("z");
        dictionary.addWord("acbe");
        dictionary.addWord("acbz");
        dictionary.addWord("abc");
        dictionary.addWord("anj");
        dictionary.addWord("zwd");
        dictionary.addWord("zww");
        dictionary.addWord("zwdr");
        dictionary.addWord("zleeaafe");

        dictionary.printAllWords();

        System.out.println("search");
        System.out.println(dictionary.searchWord("acb"));
        System.out.println(dictionary.searchWord("zleeaafe"));
        System.out.println(dictionary.searchWord("zxede"));

        System.out.println();
        System.out.println("Print All words with Prefix");
        dictionary.printAllWordsWithPrefix("a");

        System.out.println();
        System.out.println("delete");
        System.out.println(dictionary.deleteWord("anjp"));
        System.out.println(dictionary.deleteWord("acbe"));
        System.out.println(dictionary.deleteWord("zwd"));
        dictionary.printAllWords();

        System.out.println();
        System.out.println("Print All words with Prefix");
        dictionary.printAllWordsWithPrefix("a");

        System.out.println();
        System.out.println("Unique rows in boolean matrix");
        int[][] matrix = {{1,1,0,1},{1,0,0,1},{1,1,0,1}};
        PrintAllUniqueRows.printAllUniqueRowsOfBooleanMatrix(matrix);

    }

    public static void treeMain(){
        TreeNode root = TreeOperations.insertInTree(null, 5);
        TreeOperations.insertInTree(root,9);
        TreeOperations.insertInTree(root,4);
        TreeOperations.insertInTree(root,3);
        TreeOperations.insertInTree(root,2);
        TreeOperations.insertInTree(root,6);
        TreeOperations.insertInTree(root,1);
        TreeOperations.insertInTree(root,8);

        System.out.println(TreeOperations.heightOfTree(root));

        List<Integer> inOrderResult = new ArrayList<>();
        TreeTraversal.inOrderTraversal(root, inOrderResult);
        System.out.println(inOrderResult);

        List<Integer> preOrderResult = new ArrayList<>();
        TreeTraversal.preOrderTraversal(root, preOrderResult);
        System.out.println(preOrderResult);

        List<Integer> postOrderResult = new ArrayList<>();
        TreeTraversal.postOrderTraversal(root, postOrderResult);
        System.out.println(postOrderResult);

        List<TreeNode> levelOrderResult = TreeTraversal.levelOrderTraversalWithNode(root);
        System.out.println(levelOrderResult);

        List<TreeNode> zigzagLevelOrderResult = TreeTraversal.zigzagLevelOrderTraversal(root);
        System.out.println(zigzagLevelOrderResult);

        //TreeOperations.deleteFromTree(root, 6);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(root));

        //TreeOperations.deleteFromTree(root, 5);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(root));

        System.out.println(Diameter.getTreeDiameter(root));

        System.out.println(Diameter.getTreeDiameterWithOneIteration(root));

        TreeViews.printAllViews(root);

        TreeViews.printLeftAndRightViewUsingLevelOrderTraversal(root);

        System.out.println(DuplicateSubtree.DoesBTContainDuplicateSubTrees(root));

        System.out.println(DistBwTwoNodes.distBwTwoNodes(root, 8, 6));
        System.out.println(DistBwTwoNodes.distBwTwoNodes(root, 8, 2));

        List<Integer> preOrder = Arrays.asList(4,10,5,9,3,6,7,8,1);
        List<Integer> inOrder = Arrays.asList(10,9,5,3,4,7,8,6,1);
        TreeNode constructedTreeRoot = ConstructTreeFromTraversal.constructTreeFromPreOrderAndInorderTraversals(preOrder, inOrder);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(constructedTreeRoot));
        System.out.println(TreeTraversal.boundaryTraversal(constructedTreeRoot));

        List<Integer> preOrder1 = Arrays.asList(1,3,2,0,-2,-1,4,8,2,5,6);
        List<Integer> inOrder1 = Arrays.asList(2,3,-2, 0,1,8,4,2,-1,5,6);
        TreeNode constructedTreeRoot1 = ConstructTreeFromTraversal.constructTreeFromPreOrderAndInorderTraversals(preOrder1, inOrder1);
        System.out.println(TreeTraversal.levelOrderTraversalWithNode(constructedTreeRoot1));
        Set<List<TreeNode>> result = new HashSet<>();
        KSumPaths.getAllKSumPaths(constructedTreeRoot1,5, result);
        for (List<TreeNode> treeNodes : result) {
            System.out.println(treeNodes);
        }
    }
}