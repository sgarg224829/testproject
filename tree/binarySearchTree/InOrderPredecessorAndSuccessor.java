package tree.binarySearchTree;

import tree.TreeNode;

public class InOrderPredecessorAndSuccessor {

    public static Pair predecessorSuccessor(TreeNode root, int key){
        if(root != null){
            if(root.data == key){
                TreeNode predecessor = findMaxInBST(root.left);
                TreeNode successor = findMinInBST(root.right);
                return new Pair(predecessor, successor);
            }else{
                if(root.data < key){
                    Pair pair = predecessorSuccessor(root.right, key);
                    if(pair.predecessor == null){
                        pair.predecessor = root;
                    }
                    return pair;
                }else {
                    Pair pair = predecessorSuccessor(root.left, key);
                    if(pair.successor == null){
                        pair.successor = root;
                    }
                    return pair;
                }
            }
        }
        return new Pair(null, null);
    }

    public static TreeNode findMinInBST(TreeNode root){
        if(root != null){
            if(root.left != null){
                return findMinInBST(root.left);
            }else{
                return root;
            }
        }
        return null;
    }

    public static TreeNode findMaxInBST(TreeNode root){
        if(root != null){
            if(root.right != null){
                return findMaxInBST(root.right);
            }else{
                return root;
            }
        }
        return null;
    }
}
