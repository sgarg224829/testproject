package tree.binarySearchTree;

import java.util.ArrayList;
import java.util.List;

public class PreOrderToPostOrderConversion {

    public static List<Integer> preToPostConversion(List<Integer> preOrder){
        List<Integer> postOrder = new ArrayList<>();
        preToPostConversion(preOrder, 0 , preOrder.size(), postOrder);
        return postOrder;
    }

    public static void preToPostConversion(List<Integer> preOrder, int start, int end, List<Integer> postOrder){
        if(start < end){
            int root = preOrder.get(start);
            int rightSubTreeIndex = end;
            for(int i = start+1; i < end; i++){
                if(preOrder.get(i) > root){
                    rightSubTreeIndex = i;
                    break;
                }
            }
            preToPostConversion(preOrder, start+1, rightSubTreeIndex, postOrder);
            preToPostConversion(preOrder, rightSubTreeIndex, end, postOrder);
            postOrder.add(root);
        }
    }
}
