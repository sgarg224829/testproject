package tree.binarySearchTree;

import tree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class KthLargestElement {

    public static int kthLargest(TreeNode root, int K)
    {
        List<Integer> ans = new ArrayList<>();
        kthLargest(root, ans);
        return ans.get(K-1);
    }

    public static void kthLargest(TreeNode root, List<Integer> ans){
        if(root != null){
            kthLargest(root.right, ans);
            ans.add(root.data);
            kthLargest(root.left, ans);
        }
    }

    public static int kthLargestUsingIterativeInorder(TreeNode root, int K)
    {
        if(root != null){
            int count = 0;
            Stack<TreeNode> s = new Stack<>();
            s.push(root);

            TreeNode currNode = root;
            while (currNode.right != null){
                s.push(currNode.right);
                currNode = currNode.right;
            }

            while (!s.isEmpty()){
                currNode = s.pop();
                count++;
                if(count == K){
                    break;
                }
                if(currNode.left != null){
                    currNode = currNode.left;
                    s.push(currNode);
                    while (currNode.right != null){
                        s.push(currNode.right);
                        currNode = currNode.right;
                    }
                }
            }
            return currNode.data;
        }
        return -1;
    }
}
