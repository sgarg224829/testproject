package tree.binarySearchTree;

import tree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MergeBST {

    public static List<Integer> mergeBSTUsingIterativeInOrder(TreeNode root1, TreeNode root2){
        List<Integer> mergedBSTInorder = new ArrayList<>();

        Stack<TreeNode> s1 = new Stack<>();
        s1.push(root1);
        Stack<TreeNode> s2 = new Stack<>();
        s2.push(root2);

        TreeNode currNode = root1;
        while(currNode.left != null){
            s1.push(currNode.left);
            currNode = currNode.left;
        }

        currNode = root2;
        while(currNode.left != null){
            s2.push(currNode.left);
            currNode = currNode.left;
        }

        while (!s1.empty() || !s2.isEmpty()){
            if(!s1.empty() && !s2.isEmpty()){
                if(s1.peek().data < s2.peek().data){
                    currNode = s1.pop();
                    mergedBSTInorder.add(currNode.data);
                    process(currNode, s1);
                }else{
                    currNode = s2.pop();
                    mergedBSTInorder.add(currNode.data);
                    process(currNode, s2);
                }
            }else{
                if(s1.isEmpty()){
                    currNode = s2.pop();
                    mergedBSTInorder.add(currNode.data);
                    process(currNode, s2);
                }else{
                    currNode = s1.pop();
                    mergedBSTInorder.add(currNode.data);
                    process(currNode, s1);
                }
            }
        }
        return mergedBSTInorder;
    }

    private static void process(TreeNode currNode, Stack<TreeNode> s){
        if(currNode.right != null){
            currNode = currNode.right;
            s.push(currNode);
            while(currNode.left != null){
                s.push(currNode.left);
                currNode = currNode.left;
            }
        }
    }
    public static List<Integer> mergeBST(TreeNode root1, TreeNode root2){
        List<Integer> ans1 = new ArrayList<>();
        List<Integer> ans2 = new ArrayList<>();
        inOrder(root1, ans1);
        inOrder(root2, ans2);
        return merge(ans1, ans2);
    }

    private static List<Integer> merge(List<Integer> ans1, List<Integer> ans2) {
        List<Integer> combinedList = new ArrayList<>();
        int i = 0, j = 0;
        while(i< ans1.size() && j < ans2.size()){
            if(ans1.get(i) < ans2.get(j)){
                combinedList.add(ans1.get(i));
                i++;
            }else{
                combinedList.add(ans2.get(j));
                j++;
            }
        }
        while(i < ans1.size()){
            combinedList.add(ans1.get(i));
            i++;
        }
        while(j < ans2.size()){
            combinedList.add(ans2.get(j));
            j++;
        }
        return combinedList;
    }

    public static void inOrder(TreeNode root, List<Integer> ans){
        if(root != null){
            inOrder(root.left, ans);
            ans.add(root.data);
            inOrder(root.right, ans);
        }
    }
}
