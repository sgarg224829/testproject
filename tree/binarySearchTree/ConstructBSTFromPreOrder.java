package tree.binarySearchTree;

import tree.TreeNode;

import java.util.Arrays;
import java.util.List;

public class ConstructBSTFromPreOrder {

    public static TreeNode constructBSTFromPreOrder(List<Integer> preOrder){
        return constructBSTFromPreOrder(preOrder, 0 , preOrder.size());
    }
    public static TreeNode constructBSTFromPreOrder(List<Integer> preOrder, int start, int end){
        if(start < end){
            TreeNode root = new TreeNode(preOrder.get(start));
            int rightSubTreeIndex = end;
            for(int i = start+1; i < end ; i++){
                if(preOrder.get(i) > root.data){
                    rightSubTreeIndex = i;
                    break;
                }
            }
            root.left = constructBSTFromPreOrder(preOrder, start+1, rightSubTreeIndex);
            root.right = constructBSTFromPreOrder(preOrder, rightSubTreeIndex, end);
            return root;
        }
        return null;
    }
}
