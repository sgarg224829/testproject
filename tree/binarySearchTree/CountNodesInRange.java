package tree.binarySearchTree;

import tree.TreeNode;

public class CountNodesInRange {

    public static int getCount(TreeNode root, int l, int h)
    {
        //Your code here
        int count = 0;
        if(root != null && h >= l){
            if(root.data > l && root.data < h){
                count++;
                count = count + getCount(root.left, l, h);
                count = count + getCount(root.right, l, h);
            }else if(root.data <= l){
                if(root.data == l){
                    count++;
                }
                count = count + getCount(root.right, l, h);
            }else{
                if(root.data == h){
                    count++;
                }
                count = count + getCount(root.left, l, h);
            }
        }
        return count;
    }
}
