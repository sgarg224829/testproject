package tree.binarySearchTree;

import tree.TreeNode;

public class LargestBST {

    public static int largestBst(TreeNode root) {
        // Write your code here
        if (root != null) {
            int left = largestBst(root.left);
            int right = largestBst(root.right);

            int ans = Math.max(left, right);
            return isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE) ? left + right + 1 : ans;
        }
        return 0;
    }

    private static boolean isBST(TreeNode root, int min, int max){
        if(root == null){
            return true;
        }

        return root.data >= min && root.data < max &&
                isBST(root.left, min, root.data) && isBST(root.right, root.data, max);
    }
}
