package tree.binarySearchTree;

import tree.TreeNode;

public class BSTOperations {

    public static void insertInBST(TreeNode root, int data){
        if(root == null){
            root = new TreeNode(data);
        }else{
            if(root.data <= data){
                insertInBST(root.left, data);
            }else {
                insertInBST(root.right, data);
            }
        }
    }

    public static TreeNode deleteFromTree(TreeNode root, int data){
        if(root != null){
            if(root.data == data){
                //check if leaf node
                if(root.left == null && root.right == null){
                    root = null;
                } else if(root.left != null && root.right != null) {

                    //find max from left & its parent
                    TreeNode parent = root;
                    TreeNode maxNodeInLeft = root.left;
                    while(maxNodeInLeft.right != null){
                        parent = maxNodeInLeft;
                        maxNodeInLeft = maxNodeInLeft.right;
                    }

                    if(parent == root){
                        root.data = maxNodeInLeft.data;
                        parent.left = maxNodeInLeft.left;
                    }else{
                        root.data = maxNodeInLeft.data;
                        parent.right = maxNodeInLeft.left;
                    }
                }else{
                    //Only one child
                    if(root.left != null){
                        root = root.left;
                    }else {
                        root = root.right;
                    }
                }
            }else{
                deleteFromTree(root.left, data);
                deleteFromTree(root.right, data);
            }
            return root;
        }
        return null;
    }

    public static TreeNode findMinInBST(TreeNode root){
        if(root != null){
            if(root.left != null){
                return findMinInBST(root.left);
            }else{
                return root;
            }
        }
        return null;
    }

    public static TreeNode findMaxInBST(TreeNode root){
        if(root != null){
            if(root.right != null){
                return findMaxInBST(root.right);
            }else{
                return root;
            }
        }
        return null;
    }

    public static TreeNode searchInBST(TreeNode root, int data){
        if(root != null){
            if(root.data == data){
                return root;
            } else if (root.data < data) {
                return searchInBST(root.right, data);
            }else{
                return searchInBST(root.left, data);
            }
        }
        return null;
    }
}
