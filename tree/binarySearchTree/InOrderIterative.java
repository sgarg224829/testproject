package tree.binarySearchTree;

import tree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class InOrderIterative {

    public static List<TreeNode> inOrderIterative(TreeNode root){
        List<TreeNode> inOrderTraversal = new ArrayList<>();
        if(root != null){
            Stack<TreeNode> s = new Stack<>();
            s.push(root);

            TreeNode currNode = root;
            while (currNode.left!= null){
                s.push(currNode.left);
                currNode = currNode.left;
            }
            while (!s.empty()){
                currNode = s.pop();
                inOrderTraversal.add(currNode);
                if(currNode.right != null){
                    s.push(currNode.right);
                    currNode = currNode.right;
                    while (currNode.left!= null){
                        s.push(currNode.left);
                        currNode = currNode.left;
                    }
                }
            }

        }
        return inOrderTraversal;
    }
}
