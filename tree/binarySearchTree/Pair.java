package tree.binarySearchTree;

import tree.TreeNode;

public class Pair
{
        TreeNode predecessor;
        TreeNode successor;

        public Pair(TreeNode predecessor, TreeNode successor){
                this.predecessor = predecessor;
                this.successor = successor;
        }

        @Override
        public String toString() {
                return predecessor.data + "," + successor.data;
        }
}
