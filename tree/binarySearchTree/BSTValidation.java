package tree.binarySearchTree;

import tree.TreeNode;

public class BSTValidation {

    public static boolean isBST(TreeNode root){
        return isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static boolean isBST(TreeNode root, int min, int max){
        if(root == null){
            return true;
        }else{
            return (root.data > min) && (root.data < max) && isBST(root.left, min, root.data) && isBST(root.right, root.data, max);
        }
    }
}
