package tree.binarySearchTree;

import tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class PairSum {

    public int isPairPresent(TreeNode root, int target)
    {
        // Write your code here
        List<Integer> inorderTraversal = new ArrayList<>();
        inorder(root, inorderTraversal);
        int i = 0;
        int j = inorderTraversal.size()-1;
        while(i < j){
            int sum = inorderTraversal.get(i) + inorderTraversal.get(j);
            if(sum == target){
                return 1;
            }else if(sum < target){
                i++;
            }else{
                j--;
            }
        }
        return 0;
    }

    public void inorder(TreeNode root, List<Integer> result){
        if(root != null){
            inorder(root.left, result);
            result.add(root.data);
            inorder(root.right, result);
        }
    }
}
