package tree.binaryTree;

import tree.TreeNode;

import java.util.List;

public class ConstructTreeFromTraversal {

    public static TreeNode constructTreeFromPreOrderAndInorderTraversals(List<Integer> preOrder, List<Integer> inOrder){
        if(preOrder.isEmpty() || inOrder.isEmpty()){
            return null;
        }

        int first = preOrder.get(0);
        int pivot = inOrder.indexOf(first);
        TreeNode newNode = new TreeNode(first);

        newNode.setLeft(pivot > 0 ? constructTreeFromPreOrderAndInorderTraversals(preOrder.subList(1, pivot+1), inOrder.subList(0, pivot)) : null);
        newNode.setRight(pivot+1  < inOrder.size() ? constructTreeFromPreOrderAndInorderTraversals(preOrder.subList(pivot+1, preOrder.size()), inOrder.subList(pivot+1, inOrder.size())) : null);
        return newNode;
    }
}
