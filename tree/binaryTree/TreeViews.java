package tree.binaryTree;

import tree.TreeNode;

import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class TreeViews {

     private static class NodeCoordinate{
         int x;
         int y;

         int data;
         NodeCoordinate(int x, int y, int data){
             this.x = x;
             this.y = y;
             this.data = data;
         }

         public int getX() {
             return x;
         }
         public int getY() {
             return y;
         }

         public int getData(){
             return data;
         }
     }

     public static void printLeftAndRightViewUsingLevelOrderTraversal(TreeNode root){
         if(root != null){
             LinkedList<TreeNode> queue = new LinkedList<>();
             List<TreeNode> leftView = new ArrayList<>();
             List<TreeNode> rightView = new ArrayList<>();
             queue.add(root);
             queue.add(null);
             leftView.add(root);

             while(!queue.isEmpty()){
                 TreeNode firstNode = queue.removeFirst();
                 if(firstNode != null){
                     if(firstNode.getLeft() != null){
                         queue.add(firstNode.getLeft());
                     }
                     if(firstNode.getRight() != null){
                         queue.add(firstNode.getRight());
                     }
                     if(queue.getFirst() == null){
                         rightView.add(firstNode);
                     }
                 }else{
                     if(!queue.isEmpty()){
                         leftView.add(queue.getFirst());
                         queue.add(null);
                     }
                 }
             }

             System.out.print("Left view of binary tree is : ");
             System.out.println(leftView);

             System.out.print("Right view of binary tree is : ");
             System.out.println(rightView);
         }
     }

     public static void printAllViews(TreeNode root){
         Map<TreeNode, NodeCoordinate> map = new HashMap<>();
         prepareNodeCoordinateMap(root, null, map);

         TreeMap<Integer, List<NodeCoordinate>> groupByLevels = map.values().stream().collect
                 (groupingBy(NodeCoordinate::getX, TreeMap::new, toList()));
         TreeMap<Integer, List<NodeCoordinate>> groupByVerticals = map.values().stream().collect
                 (groupingBy(NodeCoordinate::getY, TreeMap::new, toList()));

         List<Integer> leftView = new ArrayList<>();
         for(Map.Entry<Integer, List<NodeCoordinate>> entry : groupByLevels.entrySet()){
             List<NodeCoordinate> levelNodes = entry.getValue();
             int minY = levelNodes.stream().mapToInt(NodeCoordinate::getY).min().getAsInt();
             leftView.addAll(levelNodes.stream().filter(nodeCoordinate -> nodeCoordinate.getY() == minY)
                     .map(NodeCoordinate::getData).toList());
         }
         System.out.print("Left view of binary tree is : ");
         System.out.println(leftView);

         List<Integer> rightView = new ArrayList<>();
         for(Map.Entry<Integer, List<NodeCoordinate>> entry : groupByLevels.entrySet()){
             List<NodeCoordinate> levelNodes = entry.getValue();
             int maxY = levelNodes.stream().mapToInt(NodeCoordinate::getY).max().getAsInt();
             rightView.addAll(levelNodes.stream().filter(nodeCoordinate -> nodeCoordinate.getY() == maxY)
                     .map(NodeCoordinate::getData).toList());
         }
         System.out.print("Right View of binary tree is : ");
         System.out.println(rightView);

         List<Integer> topView = new ArrayList<>();
         for(Map.Entry<Integer, List<NodeCoordinate>> entry : groupByVerticals.entrySet()){
             List<NodeCoordinate> verticalNodes = entry.getValue();
             int minX = verticalNodes.stream().mapToInt(NodeCoordinate::getX).min().getAsInt();
             topView.addAll(verticalNodes.stream().filter(nodeCoordinate -> nodeCoordinate.getX() == minX)
                     .map(NodeCoordinate::getData).toList());
         }
         System.out.print("Top View of binary tree is : ");
         System.out.println(topView);

         List<Integer> bottomView = new ArrayList<>();
         for(Map.Entry<Integer, List<NodeCoordinate>> entry : groupByVerticals.entrySet()){
             List<NodeCoordinate> verticalNodes = entry.getValue();
             int maxX = verticalNodes.stream().mapToInt(NodeCoordinate::getX).max().getAsInt();
             bottomView.addAll(verticalNodes.stream().filter(nodeCoordinate -> nodeCoordinate.getX() == maxX)
                     .map(NodeCoordinate::getData).toList());
         }
         System.out.print("Bottom view of binary tree is : ");
         System.out.println(bottomView);
     }

     private static void prepareNodeCoordinateMap(TreeNode currentNode, TreeNode parentNode, Map<TreeNode, NodeCoordinate> map){
         if(currentNode != null){
             if(parentNode == null){
                 map.put(currentNode, new NodeCoordinate(0,0, currentNode.getData()));
             }else{
                 NodeCoordinate nodeCoordinate = map.get(parentNode);
                 if(parentNode.getLeft().equals(currentNode)){
                     map.put(currentNode, new NodeCoordinate(nodeCoordinate.x + 1, nodeCoordinate.y - 1, currentNode.getData()));
                 }else{
                     map.put(currentNode, new NodeCoordinate(nodeCoordinate.x + 1, nodeCoordinate.y + 1, currentNode.getData()));
                 }
             }
             if(currentNode.getLeft() != null){
                 prepareNodeCoordinateMap(currentNode.getLeft(), currentNode, map);
             }
             if(currentNode.getRight() != null){
                 prepareNodeCoordinateMap(currentNode.getRight(), currentNode, map);
             }
         }
     }
}
