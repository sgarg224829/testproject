package tree.binaryTree;

import tree.TreeNode;
import tree.TreeOperations;

public class Diameter {

    public static int getTreeDiameter(TreeNode root){
        if(root == null){
            return 0;
        }

        int lDiameter = getTreeDiameter(root.getLeft());
        int rDiameter = getTreeDiameter(root.getRight());

        int diameter = TreeOperations.heightOfTree(root.getLeft()) + TreeOperations.heightOfTree(root.getRight());
        return Math.max(Math.max(lDiameter, rDiameter), diameter);
    }

    public static int getTreeDiameterWithOneIteration(TreeNode root){
        return getTreeDiameterWithHeight(root).diameter;
    }

    private static Combine getTreeDiameterWithHeight(TreeNode root){
        if(root == null){
            return new Combine(0, 0);
        }

        Combine left = getTreeDiameterWithHeight(root.getLeft());
        Combine right = getTreeDiameterWithHeight(root.getRight());

        int height = 1 + Math.max(left.height, right.height);
        int diameter = Math.max(Math.max(left.diameter, right.diameter), left.height + right.height );

        return new Combine(height, diameter);
    }

    private static class Combine{
        int height;

        int diameter;

        Combine(int height, int diameter){
            this.height = height;
            this.diameter = diameter;
        }
    }
}
