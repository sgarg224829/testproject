package tree.binaryTree;

import tree.TreeException;
import tree.TreeNode;

public class LCA {

    public static TreeNode findLCA(TreeNode root, TreeNode x , TreeNode y){
        if(root == null || x == null || y == null){
            return null;
        }

        if(!(isNodePresent(root, x) && isNodePresent(root, y))){
            return null;
        }

        if(root == x || root == y){
            return root;
        }

        boolean xInLeft = isNodePresent(root.getLeft(), x);
        boolean xInRight = isNodePresent(root.getRight(), x);
        boolean yInLeft = isNodePresent(root.getLeft(), y);
        boolean yInRight = isNodePresent(root.getRight(), y);

        if(!(xInLeft || xInRight) || !(yInLeft || yInRight)){
            throw new TreeException("Node x or y not present in tree");
        } else if ((xInLeft && yInRight) || (xInRight && yInLeft)) {
           return root;
        }else if(xInLeft && yInLeft){
            return findLCA(root.getLeft(), x, y);
        }else {
            return findLCA(root.getRight(), x, y);
        }
    }

    public static TreeNode findLCA2(TreeNode root, TreeNode x , TreeNode y){
        if(root == null || x == null || y == null){
            return null;
        }

        if(root == x || root == y){
            return root;
        }

        TreeNode left = findLCA2(root.getLeft(), x, y);
        TreeNode right = findLCA2(root.getRight(), x, y);

        //result
        if(left == null) {
            return right;
        }
        else if(right == null) {
            return left;
        }
        else { //both left and right are not null, we found our result
            return root;
        }
    }

    public static TreeNode findLCA3(TreeNode root, int x , int y){
        if(root == null){
            return null;
        }

        if(root.getData() == x || root.getData() == y){
            return root;
        }

        TreeNode left = findLCA3(root.getLeft(), x, y);
        TreeNode right = findLCA3(root.getRight(), x, y);

        //result
        if(left == null) {
            return right;
        }
        else if(right == null) {
            return left;
        }
        else { //both left and right are not null, we found our result
            return root;
        }
    }

    private static boolean isNodePresent(TreeNode root, TreeNode p){
        if(root == null){
            return false;
        }
        return root.equals(p) || isNodePresent(root.getLeft(), p) || isNodePresent(root.getRight(), p);
    }
}
