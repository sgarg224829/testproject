package tree.binaryTree;

import tree.TreeNode;

public class SumTree {

    public boolean isSumTree(TreeNode root){
        if(root == null){
            return true;
        }
        if(root.getLeft() == null && root.getRight() == null){
            return true;
        }

        int leftSum = 0, rightSum = 0;
        if(root.getLeft() != null){
            leftSum = isLeafNode(root.getLeft()) ? root.getLeft().getData() : 2 * root.getLeft().getData();
        }
        if(root.getRight() != null){
            rightSum = isLeafNode(root.getRight()) ? root.getRight().getData() : 2 * root.getRight().getData();
        }
        boolean isCurrentNodeSumTree = (root.getData() == leftSum + rightSum);
        return isCurrentNodeSumTree && isSumTree(root.getLeft()) && isSumTree(root.getRight());
    }

    boolean isLeafNode(TreeNode node){
        return node != null && node.getLeft() == null && node.getRight() == null;
    }
}
