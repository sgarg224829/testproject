package tree.binaryTree;

import tree.TreeNode;

public class MirrorTree {

    public static boolean areMirrorTrees(TreeNode root1, TreeNode root2){
        if(root1 == null && root2 == null){
            return true;
        }else if(root1 != null && root2 != null){
            return root1.getData() == root2.getData() && areMirrorTrees(root1.getLeft(), root2.getRight()) &&
                    areMirrorTrees(root1.getLeft(), root2.getRight());
        }else {
            return false;
        }
    }
}
