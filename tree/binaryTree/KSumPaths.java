package tree.binaryTree;

import tree.TreeNode;

import java.util.*;

public class KSumPaths {

    public static void getAllKSumPaths(TreeNode root, int k, Set<List<TreeNode>> result){
        if(root == null){
            return;
        }

        getAllKSumPathsContainingRoot(root, k, result);
        getAllKSumPaths(root.getLeft(), k, result);
        getAllKSumPaths(root.getRight(), k,  result);
    }

    private static void getAllKSumPathsContainingRoot(TreeNode root, int k, Set<List<TreeNode>> result){
        if(root == null){
            return;
        }

        if(root.getData() == k){
            ArrayList<TreeNode> list = new ArrayList<>();
            list.add(root);
            result.add(list);
        }

        Set<List<TreeNode>> leftResult = new HashSet<>();
        getAllKSumPathsContainingRoot(root.getLeft(), k-root.getData(), leftResult);

        Set<List<TreeNode>> rightResult = new HashSet<>();
        getAllKSumPathsContainingRoot(root.getRight(), k-root.getData(), rightResult);

        if(!leftResult.isEmpty()){
            leftResult.forEach(path -> path.add(0, root));
            result.addAll(leftResult);
        }

        if(!rightResult.isEmpty()){
            rightResult.forEach(path -> path.add(0, root));
            result.addAll(rightResult);
        }
    }
}
