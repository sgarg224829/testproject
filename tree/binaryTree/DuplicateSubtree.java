package tree.binaryTree;

import tree.TreeNode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class DuplicateSubtree {

    public static boolean DoesBTContainDuplicateSubTrees(TreeNode root){
        List<String> serialKeys = new ArrayList<>();
        serializeTree(root, serialKeys);
        return serialKeys.size() != new HashSet<>(serialKeys).size();
    }

    private static String serializeTree(TreeNode root, List<String> serialKeys){
        StringBuilder s = new StringBuilder();
        if(root == null){
          s.append("$");
          return s.toString();
        }

        String lStr = serializeTree(root.getLeft(), serialKeys);
        String rStr = serializeTree(root.getRight(), serialKeys);

        s.append(root.getData()).append("#").append(lStr).append('#').append(rStr);

        if(s.length() > 7){
            serialKeys.add(s.toString());
        }

        return s.toString();
    }

    private static boolean isIdentical(TreeNode root1, TreeNode root2){
        if(root1 == null && root2 == null){
            return true;
        }

        if(root1 != null & root2 != null){
            return root1.getData() == root2.getData() && isIdentical(root1.getLeft(), root2.getLeft()) &&
                    isIdentical(root1.getRight(), root2.getRight());
        }else {
            return false;
        }
    }
}
