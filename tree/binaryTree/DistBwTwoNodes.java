package tree.binaryTree;

import tree.TreeNode;

public class DistBwTwoNodes {

    public static int distBwTwoNodes(TreeNode root, int x, int y){
        if(root == null || x == y){
            return 0;
        }

        TreeNode lcaNode = LCA.findLCA3(root, x, y);
        if(lcaNode != null){
            return findDistanceFromLCA(lcaNode, x, 0) + findDistanceFromLCA(lcaNode, y, 0);
        }else {
            return 0;
        }
    }

    private static int findDistanceFromLCA(TreeNode lcaNode, int p, int dist){

        if(lcaNode == null){
            return -1;
        }

        if(lcaNode.getData() == p){
            return dist;
        }

        int left = findDistanceFromLCA(lcaNode.getLeft(), p, dist+1);
        if(left == -1){
            return findDistanceFromLCA(lcaNode.getRight(), p, dist+1);
        }
        return left;
    }
}
