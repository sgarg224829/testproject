package tree;

import java.util.List;

public class TreeOperations {

    public static TreeNode insertInTree(TreeNode root, int data){
        //insert element in the tree considering it as binary tree & in levelOrder
        TreeNode newNode = new TreeNode(data);
        if(root == null){
            return newNode;
        }

        List<TreeNode> levelOrderTraversal = TreeTraversal.levelOrderTraversalWithNode(root);
        for(TreeNode node : levelOrderTraversal){
            if(node.getLeft() == null){
                node.setLeft(newNode);
                return root;
            } else if (node.getRight() == null) {
                node.setRight(newNode);
                return root;
            }
        }

        throw new TreeException("Level Order traversal of tree is not correct");
    }

    public static TreeNode deleteFromTree(TreeNode root, int data){

        //delete Element from Tree = Replace to be deleted element with bottom rightmost element & delete bottom rightmost node
        if(root == null){
            throw new TreeException("Tree is null");
        }

        List<TreeNode> levelOrderTraversal = TreeTraversal.levelOrderTraversalWithNode(root);
        boolean elementFound = false;
        TreeNode lastNode = levelOrderTraversal.get(levelOrderTraversal.size()-1);
        for(TreeNode treeNode : levelOrderTraversal){
            if(treeNode.getData() == data){
                elementFound = true;
                int temp = lastNode.getData();
                lastNode.setData(treeNode.getData());
                treeNode.setData(temp);
                break;
            }
        }

        if(!elementFound){
            throw new TreeException("Element not found in tree");
        }else {
            TreeNode parentNode = findParentNode(root, lastNode);
            if(parentNode == null){
                root = null;
            }else if(parentNode.getLeft() == lastNode){
                parentNode.setLeft(null);
            }else if(parentNode.getRight() == lastNode){
                parentNode.setRight(null);
            }else{
                throw new TreeException("Parent Node is not correct");
            }
        }

        return root;
    }

    public static TreeNode findParentNode(TreeNode root, TreeNode node){
        if(root == node){
            return null;
        }
        if(root.getLeft() == null && root.getRight() == null){
            return null;
        }
        if(root.getLeft() != null && root.getLeft() == node){
            return root;
        }
        if (root.getRight() != null && root.getRight() == node){
            return root;
        }
        TreeNode inLeft = findParentNode(root.getLeft(), node);
        if(inLeft != null){
            return inLeft;
        }else{
            return findParentNode(root.getRight(), node);
        }
    }

    public static int heightOfTree(TreeNode root){
        if(root == null){
            return 0;
        }else{
            return 1+ Math.max(heightOfTree(root.getLeft()), heightOfTree(root.getRight()));
        }
    }
}
