package tree;

import java.util.*;
import java.util.stream.Collectors;

public class TreeTraversal {

    public static void preOrderTraversal(TreeNode root, List<Integer> result){
        if(root !=  null){
            result.add(root.getData());
            if(root.getLeft() != null){
                preOrderTraversal(root.getLeft(), result);
            }
            if(root.getRight() != null){
                preOrderTraversal(root.getRight(), result);
            }
        }
    }

    public static void postOrderTraversal(TreeNode root, List<Integer> result){
        if(root !=  null){
            if(root.getLeft() != null){
                postOrderTraversal(root.getLeft(), result);
            }
            if(root.getRight() != null){
                postOrderTraversal(root.getRight(), result);
            }
            result.add(root.getData());
        }
    }

    public static List<Integer> inOrderTraversal(TreeNode root){
        ArrayList<Integer> result = new ArrayList<>();
        inOrderTraversal(root, result);
        return result;
    }

    public static void inOrderTraversal(TreeNode root, List<Integer> result){
        if(root !=  null){
            if(root.getLeft() != null){
                inOrderTraversal(root.getLeft(), result);
            }
            result.add(root.getData());
            if(root.getRight() != null){
                inOrderTraversal(root.getRight(), result);
            }
        }
    }
    public static List<TreeNode> levelOrderTraversalWithNode(TreeNode root){
        List<TreeNode> result = new ArrayList<>();
        LinkedList<TreeNode> queue = new LinkedList<>();
        if(root != null){
            queue.add(root);
            while (!queue.isEmpty()){
                TreeNode node = queue.removeFirst();
                if(node.getLeft() != null){
                   queue.add(node.getLeft());
                }
                if(node.getRight() != null){
                    queue.add(node.getRight());
                }
                result.add(node);
            }
        }
        return result;
    }

    public static List<TreeNode> zigzagLevelOrderTraversal(TreeNode root){
        List<TreeNode> result = new ArrayList<>();
        LinkedList<TreeNode> queue = new LinkedList<>();
        Stack<TreeNode> stack = new Stack<>();
        boolean leftToRight = false;
        if(root != null){
            queue.add(root);
            queue.add(null);
            while (!queue.isEmpty()){
                TreeNode node = queue.removeFirst();
                if(node != null){
                    if(node.getLeft() != null){
                        queue.add(node.getLeft());
                    }
                    if(node.getRight() != null){
                        queue.add(node.getRight());
                    }
                    if(leftToRight){
                        result.add(node);
                    }else{
                        stack.push(node);
                    }
                }else{
                    while (!stack.isEmpty()){
                        result.add(stack.pop());
                    }
                    if(!queue.isEmpty()){
                        queue.add(null);
                        leftToRight = !leftToRight;
                    }
                }
            }
        }
        return result;
    }

    public static List<Integer> boundaryTraversal(TreeNode root)
    {
        LinkedHashSet<TreeNode> result = new LinkedHashSet<>();
        boundaryTraversal(root, result);
        return result.stream().map(TreeNode::getData).collect(Collectors.toList());
    }

    public static void boundaryTraversal(TreeNode root, LinkedHashSet<TreeNode> result){
        if(root != null){

            //getAll Left nodes
            result.add(root);
            TreeNode node = root.getLeft();
            while (node != null){
                result.add(node);
                if(!(node.getLeft() == null && node.getRight() == null)){
                    if(node.getLeft() != null){
                        node = node.getLeft();
                    }else{
                        node = node.getRight();
                    }
                }else{
                    break;
                }
            }

            getAllLeafNodes(root, result);

            //getAll Right nodes
            node = root.getRight();
            Stack<TreeNode> stack = new Stack<>();
            while (node != null){
                stack.push(node);
                if(!(node.getLeft() == null && node.getRight() == null)){
                    if(node.getRight() != null){
                        node = node.getRight();
                    }else{
                        node = node.getLeft();
                    }
                }else{
                    break;
                }
            }
            while (!stack.isEmpty()){
                result.add(stack.pop());
            }
        }
    }

    private static void getAllLeafNodes(TreeNode root, LinkedHashSet<TreeNode> result){
        if(root != null){
            if(root.getLeft() == null && root.getRight() == null){
                result.add(root);
            }else{
                getAllLeafNodes(root.getLeft(), result);
                getAllLeafNodes(root.getRight(), result);
            }
        }
    }
}
